# RustServiceIAC

This repo holds IAC for the provisioning of a GKS cluster

# How to use

### Configure GCP account

First make sure you installed gcloud, kubectl and terraform CLIs. On mac:

```bash
brew update
brew install terraform
brew install kubectl
curl https://sdk.cloud.google.com | bash
```

Then restart your shell session for the changes to take effect. You can now init your gcloud CLI using:

```bash
gcloud init
```

We now need to enable gcloud APIs for Computing and Kubernetes Engine:

```bash
gcloud services enable compute.googleapis.com
gcloud services enable container.googleapis.com
gcloud services enable cloudresourcemanager.googleapis.com
```

and create a service account for terraform to use (you can for instance name it "terraform"):

```bash
gcloud iam service-accounts create <service_account_name>
```

Next we need to give enough permissions to this service account to perform the tasks it needs to do: (permissions from the documentation of [GKE Terraform module](https://registry.terraform.io/modules/terraform-google-modules/kubernetes-engine/google/8.1.0#configure-a-service-account)) (One permission added for enabling remote state storage and GCS backend)

```bash
gcloud projects add-iam-policy-binding <project_name> --member serviceAccount:<service_account_name>@<project_name>.iam.gserviceaccount.com --role roles/compute.viewer
gcloud projects add-iam-policy-binding <project_name> --member serviceAccount:<service_account_name>@<project_name>.iam.gserviceaccount.com --role roles/container.clusterAdmin
gcloud projects add-iam-policy-binding <project_name> --member serviceAccount:<service_account_name>@<project_name>.iam.gserviceaccount.com --role roles/container.developer
gcloud projects add-iam-policy-binding <project_name> --member serviceAccount:<service_account_name>@<project_name>.iam.gserviceaccount.com --role roles/iam.serviceAccountAdmin
gcloud projects add-iam-policy-binding <project_name> --member serviceAccount:<service_account_name>@<project_name>.iam.gserviceaccount.com --role roles/iam.serviceAccountUser
gcloud projects add-iam-policy-binding <project_name> --member serviceAccount:<service_account_name>@<project_name>.iam.gserviceaccount.com --role roles/resourcemanager.projectIamAdmin
gcloud projects add-iam-policy-binding <project_name> --member serviceAccount:<service_account_name>@<project_name>.iam.gserviceaccount.com --role roles/storage.objectAdmin
```

We can now download our service-account credentials file using command:

```bash
gcloud iam service-accounts keys create terraform-gke-keyfile.json --iam-account=<service_account_name>@<project_name>.iam.gserviceaccount.com
```

**DONT FORGET TO CHECK IT OUT OF VERSION CONTROL**

### Configure Terraform

We will need to store the state of our Terraformed cluster (see Terraform's handling of state). For this, we will use a GCS bucket as it is the easiest way to to. Let's create one and activate versioning on it :

```bash
gsutil mb -p <project_name> -c regional -l <location> gs://<bucket_name>/
gsutil versioning set on gs://<bucket_name>/
```

Now we can create our `terraform.tf` as in this repo. It specifies we'll use the GCS backend to store our state and point to the bucket.

You can now run

```bash
terraform init
// or if first one yields errors regarding backend
terraform init -reconfigure
```

### Describe our cluster via Terraform

First create `providers.tf` file as in the repo and specify the google and google-beta providers, using version 3.x.x.

Then create the `main.tf` (that uses gke module), the `variables.tf` and the `variables.auto.tfvars`(which will be the only file you need to edit to parametrize your cluster) as they are in this repo.

Now you need to rerun terraform init to install gke module:

```bash
terraform init -upgrade
```

Now you can plan and then apply:

```bash
terraform plan
```

```bash
terraform apply
```

After this you can check that your cluster is created and retrieve your kubectl config file:

```bash
gcloud container clusters list
gcloud container clusters get-credentials gke-cluster
```

To destroy your cluster just run:

```bash
terraform destroy
```

You can then recreate it in minutes by just running the above commands thanks to the magic of IAC :)
