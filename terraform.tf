terraform {
  backend "gcs" {
    credentials = "./terraform-gke-keyfile.json"
    bucket      = "rustserviceiac"
    prefix      = "terraform/state"
  }
}
