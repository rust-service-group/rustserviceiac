provider "google" {
  version     = "3.15.0"
  credentials = file(var.credentials)
  project     = var.project_id
  region      = var.location
}

provider "google-beta" {
  version     = "3.15.0"
  credentials = file(var.credentials)
  project     = var.project_id
  region      = var.location
}
